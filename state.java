
/*@Author : Suvam Saha
the following java program demonstrate how to compare the state
of two objects without using comparator*/



import java.lang.reflect.Field;
import java.lang.Class;
import java.lang.Exception;

class abc{
 private String a,b;
 
 //constructor of abc
 abc(String a, String b){
	this.a = a;
	this.b = b;
 
 }

 }
 class def{
 private long d,e,f;
 //constructor of def
 def(int d, int e , int f){
	this.d = d;
	this.e = e;
	this.f = f;
 
 }
 }
 class ghi extends def{ // extends def class
	private long g,h,i;
	
	//constructor of ghi
	ghi(int d, int e, int f,int g, int h, int i){
		super(d,e,f);    //call constructor of superclass def
		this.g = g;
		this.h = h;
		this.i = i;	
	}
 
 }

 //class that holds compareto method
class chkstate  {

		// make compareto utility method as declare it public static 
		public static boolean compareto(Object a, Object b) throws IllegalAccessException {
			
			boolean flg = false;
	
			//check whether object a is null or not and also from different class
			if(a == null || a.getClass() != b.getClass())
			{
				flg=false;
				
			}
			//check whether classes of these objects are equal or not
			else if(a.getClass().equals(b.getClass()))
			{
				Field[] f1 = a.getClass().getDeclaredFields();
				for(Field f:f1){
					f.setAccessible(true);			//to access private attributes
					if(f.get(a).equals(f.get(b))){
						flg = true;
					}else{
					
						flg = false;
						
						break;
					}			
				}
			}
			//block that compare state of objects in case of base and parent class 
			if(flg){
					Class c = a.getClass();
					c = c.getSuperclass();
				//	while(c != null){
					Field[] fl = a.getClass().getDeclaredFields();
					for(Field f:fl){
					f.setAccessible(true);
					if(f.get(a).equals(f.get(b))){
						flg = true;
					}else{
					
						flg = false;
						break;
					}	
					}
					c = c.getSuperclass();
				
			//	}
			
			
			}
	
		
		
		if(flg == true){
		
			System.out.println("true");
		
		}else{
				System.out.println("false");
		
		}

return flg;
}

}
//class that holds main method
class state{

	public static void main(String[] arg) throws Exception{
	
	try{ //exception handled
		//initialize objects of the classes
		abc a1 = new abc("abc","edf");
		abc a2 = new abc("abc","edf");
		def d1 = new def(1,222,19);
		ghi d2 = new ghi(1,22,19,5,6,7);
		def d3 = new def(1,22,19);

		// compare states
		chkstate.compareto(a1,a2);
		chkstate.compareto(d2,d3);
		chkstate.compareto(d2,null);
	}catch(Exception e){
		System.out.println("Error!!");
	}	
	
}
}
